#-------------------------------------------------
#
# Project created by QtCreator 2020-03-12T10:34:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Takuzu
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/main.cpp \
        src/MainWindow.cpp \
        src/MainMenuView.cpp \
        src/ViewManager.cpp \
        src/GridSelectionView.cpp \
        src/MainMenuPresenter.cpp \
        src/Presenter.cpp \
        src/GridSelectionPresenter.cpp \
        src/GameView.cpp \
        src/GamePresenter.cpp \
        src/Game/TakuzuGridLoader.cpp \
    src/GameBoard.cpp \
    src/Game/TakuzuGame.cpp \
    src/Game/TakuzuGrid.cpp \
    src/LineCounter.cpp \
    src/ColumnCounter.cpp \
    src/SquareWidget.cpp \
    src/GameObserver.cpp \
    src/Command/TakuzuGameCommand.cpp \
    src/Command/NextPawn.cpp \
    src/Command/PreviousPawn.cpp \
    src/Command/CommandStack.cpp

HEADERS += \
        include/MainWindow.h \
        include/MainMenuView.h \
        include/ViewManager.h \
        include/GridSelectionView.h \
        include/MainMenuPresenter.h \
        include/Presenter.h \
        include/GridSelectionPresenter.h \
        include/GameView.h \
        include/GamePresenter.h \
        include/GameBoard.h \
        include/Game/TakuzuGridLoader.h \
        include/Game/TakuzuGrid.h \
    include/Game/TakuzuGame.h \
    include/LineCounter.h \
    include/ColumnCounter.h \
    include/SquareWidget.h \
    include/GameObserver.h \
    include/Command/Command.h \
    include/Command/TakuzuGameCommand.h \
    include/Command/NextPawn.h \
    include/Command/PreviousPawn.h \
    include/Command/CommandStack.h

FORMS += \
        ui/MainWindow.ui \
        ui/MainMenuView.ui \
        ui/GridSelectionView.ui \
        ui/GameView.ui

RESOURCES += \
        Takuzu.qrc

DISTFILES +=
