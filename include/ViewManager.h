#ifndef VIEWMANAGER_H
#define VIEWMANAGER_H

#include <QWidget>
#include <map>
#include <QString>

class MainWindow;
class QStackedWidget;

namespace Takuzu {
    enum View {
        MainMenu,
        GridSelection,
        Game
    };
}

class ViewManager
{
public:
    ViewManager(MainWindow * window, QStackedWidget * stack);
    ~ViewManager();
    void changeView(Takuzu::View newView);
    void exit();
    void setParameter(std::map<QString, int> parameters);
    std::map<QString, int> getParameter() const;

private:
    void switchToGridSelectionView();

private:
    std::map<QString, int> _parameters;
    MainWindow * _window;
    QStackedWidget * _stack;
};

#endif // VIEWMANAGER_H
