#ifndef NEXTPAWN_H
#define NEXTPAWN_H

#include "TakuzuGameCommand.h"

class TakuzuGame;
class NextPawn : public TakuzuGameCommand {
public:
    NextPawn(TakuzuGame*, unsigned int, unsigned int);
    void execute() override;
    void undo() override;
    void redo() override;

private:
    unsigned int _row;
    unsigned int _column;
};

#endif // NEXTPAWN_H
