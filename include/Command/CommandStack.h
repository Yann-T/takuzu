#ifndef COMMANDSTACK_H
#define COMMANDSTACK_H

#include <deque>

class Command;

class CommandStack {
public:
    CommandStack();
    void push(Command *);
    bool canUndo() const;
    bool canRedo() const;
    void undoLastAction();
    void redoLastUndoneAction();

private:
    std::deque<Command *> _actionStack;
    std::deque<Command *> _undoneActionStack;
};

#endif // COMMANDSTACK_H
