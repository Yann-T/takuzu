#ifndef PREVIOUSPAWN_H
#define PREVIOUSPAWN_H

#include "TakuzuGameCommand.h"

class TakuzuGame;
class PreviousPawn : public TakuzuGameCommand {
public:
    PreviousPawn(TakuzuGame*, unsigned int, unsigned int);
    void execute() override;
    void undo() override;
    void redo() override;

private:
    unsigned int _row;
    unsigned int _column;
};

#endif // PREVIOUSPAWN_H
