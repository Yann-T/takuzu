#ifndef TAKUZUGAMECOMMAND_H
#define TAKUZUGAMECOMMAND_H

#include "Command.h"

class TakuzuGame;

class TakuzuGameCommand : public Command {
public:
    TakuzuGameCommand(TakuzuGame * game);

protected:
    TakuzuGame * _game;
};

#endif // TAKUZUGAMECOMMAND_H
