#ifndef MAINMENUVIEW_H
#define MAINMENUVIEW_H

#include <QWidget>

class MainMenuPresenter;

namespace Ui {
class MainMenuView;
}

class MainMenuView : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenuView(QWidget *parent = 0);
    ~MainMenuView();
    void setPresenter(MainMenuPresenter * presenter);
public slots:
    void onPlayClick() const;
    void onTrainingClick() const;
    void onExitClick() const;

private:
    Ui::MainMenuView *ui;
    MainMenuPresenter * _presenter;
};

#endif // MAINMENUVIEW_H
