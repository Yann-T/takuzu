#ifndef GAMEPRESENTER_H
#define GAMEPRESENTER_H

#include "Presenter.h"
#include "include/Command/CommandStack.h"
#include <QTime>

class GameView;
class TakuzuGame;

class GamePresenter : public Presenter
{
public:
    GamePresenter();
    ~GamePresenter();
    void initialize() override;
    void returnToSelection() const;
    void updateTimer();
    void updateUndoRedoState();
    void updateUndoCount();
    void stopTimer();
    void setView(GameView * view);
    TakuzuGame * getGame();
    void onLeftClick(int x, int y);
    void onRightClick(int x, int y);
    void undo();
    void redo();

private:
    CommandStack _stack;
    GameView * _view;
    TakuzuGame * _game;
    QTime _time;
    int _undoCount;
};

#endif // GAMEPRESENTER_H
