#ifndef TAKUZUGRID_H
#define TAKUZUGRID_H

#include "TakuzuGrid.h"
#include <QDebug>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


namespace Takuzu {
    enum Pawn {
        White,
        Black,
        Empty,
        LockedWhite,
        LockedBlack
    };
}

class TakuzuGrid {
public:
    TakuzuGrid(unsigned short size);
    const Takuzu::Pawn & get(unsigned int row, unsigned int column) const;
    const Takuzu::Pawn & operator()(unsigned int row, unsigned int column) const;
    Takuzu::Pawn & get(unsigned int row, unsigned int column);
    Takuzu::Pawn & operator()(unsigned int row, unsigned int column);
    unsigned short size() const;

    bool notOnBorder(unsigned int) const;

    static TakuzuGrid stringToGrid(const std::string &);
    static Takuzu::Pawn charToPawn(char c);
    static char pawnToChar(Takuzu::Pawn p);

private:
    unsigned short _size;
    std::vector<Takuzu::Pawn> _grid;
};

QDebug operator<<(QDebug, const TakuzuGrid &);




#endif // TAKUZUGRID_H
