#ifndef TAKUZUGAME_H
#define TAKUZUGAME_H

#include <QObject>
#include <set>
#include <utility>
#include <functional>
#include "include/GridSelectionPresenter.h"
#include "TakuzuGrid.h"

namespace Takuzu {
    typedef std::set<std::pair<unsigned int, unsigned int>> PawnsSet;
}

class TakuzuGame : public QObject
{
    Q_OBJECT
public:
    TakuzuGame(Takuzu::Size boardSize, Takuzu::Difficulty boardDifficulty, QObject *parent = nullptr);
    void setPawnAt(Takuzu::Pawn pawn, unsigned int y, unsigned int x);
    void setNextPawnAt(unsigned int y, unsigned int x);
    void setPreviousPawnAt(unsigned int y, unsigned int x);
    std::pair<int, int> countPawnsInRow(unsigned int row) const;
    std::pair<int, int> countPawnsInColumn(unsigned int column) const;
    Takuzu::PawnsSet getIllegalPawns() const;
    Takuzu::Size getSize() const;
    TakuzuGrid * getGrid();
    bool isBalanced(std::pair<int, int>) const;
    bool isComplete() const;
    bool isLockedCell(unsigned int row, unsigned int column);

signals:
    void completed();

private:
    bool isWhite(Takuzu::Pawn) const;
    bool isBlack(Takuzu::Pawn) const;
    bool areSameColor(Takuzu::Pawn, Takuzu::Pawn) const;
    bool isBetweenTwoPawnsOfTheSameColorInRow(unsigned int y, unsigned int x) const;
    bool isBetweenTwoPawnsOfTheSameColorInColumn(unsigned int y, unsigned int x) const;
    bool areSameRow(unsigned int y1, unsigned int y2) const;
    bool areSameColumn(unsigned int x1, unsigned int x2) const;
    void insertIllegalPawnsOnRows(Takuzu::PawnsSet &) const;
    void insertIllegalPawnsOnColumns(Takuzu::PawnsSet &) const;
    void insertIllegalPawnsBetweenTwoPawnsOfTheSameColor(Takuzu::PawnsSet &) const;
    void insertIllegalPawnsInSimilarRows(Takuzu::PawnsSet &) const;
    void insertIllegalPawnsInSimilarColumns(Takuzu::PawnsSet &) const;

private:
    Takuzu::Size _size;
    Takuzu::Difficulty _difficulty;
    TakuzuGrid _grid;
};

#endif // TAKUZUGAME_H
