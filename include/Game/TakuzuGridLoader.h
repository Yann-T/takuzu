#ifndef TAKUZUGRIDLOADER_H
#define TAKUZUGRIDLOADER_H

#include "TakuzuGrid.h"
#include "include/GridSelectionPresenter.h"
#include <QString>
#include <QFile>
#include <vector>
#include <set>
#include <string>

class TakuzuGridLoader {
public:
    TakuzuGridLoader(const QString &);
    ~TakuzuGridLoader();

    static const TakuzuGridLoader & getLoaderFromParameter(Takuzu::Difficulty, Takuzu::Size);
    TakuzuGrid getGridAt(unsigned int index) const;
    TakuzuGrid getRandomGrid() const;

private:
    QFile * _gridPoolFile;
    unsigned int _gridPoolCount;
    std::vector<std::string> _gridPoolStrings;

    static std::set<QString> Easy;
    static std::set<QString> Hard;
    static std::set<QString> Six;
    static std::set<QString> Eight;
    static std::set<QString> Ten;
    static std::set<QString> getGrids(Takuzu::Difficulty);
    static std::set<QString> getGrids(Takuzu::Size);
};


#endif // TAKUZUGRIDLOADER_H
