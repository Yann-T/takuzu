#ifndef COLUMNCOUNTER_H
#define COLUMNCOUNTER_H

#include <QWidget>

class TakuzuGame;
class GameBoard;

class ColumnCounter : public QWidget
{
    Q_OBJECT
public:
    explicit ColumnCounter(QWidget *parent = nullptr);
    void setGame(TakuzuGame * game);
    void setGameBoard(GameBoard * gameBoard);
protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
signals:

public slots:

private:
    TakuzuGame * _game;
    GameBoard * _gameBoard;
};

#endif // COLUMNCOUNTER_H
