#ifndef GRIDSELECTIONPRESENTER_H
#define GRIDSELECTIONPRESENTER_H

#include "Presenter.h"

namespace Takuzu {
    enum Difficulty {
        DifficultyNotSet,
        Easy,
        Hard
    };
    enum Size {
        SizeNotSet,
        Six,
        Eight,
        Ten
    };
}

class GridSelectionView;
class GamePresenter;

class GridSelectionPresenter : public Presenter
{
public:
    GridSelectionPresenter();
    ~GridSelectionPresenter();
    void setView(GridSelectionView * view);
    void setGamePresenter(GamePresenter * gamePresenter);
    void returnToMenu() const;
    void startGame() const;
    void setDifficulty(Takuzu::Difficulty);
    void setSize(Takuzu::Size);

    void updateStartAccess() const;
    bool parametersAreSet() const;

private:
    GridSelectionView * _view;
    GamePresenter * _gamePresenter;
    Takuzu::Difficulty _difficulty;
    Takuzu::Size _size;
};

#endif // GRIDSELECTIONPRESENTER_H
