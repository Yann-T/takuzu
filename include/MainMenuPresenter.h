#ifndef MAINMENUPRESENTER_H
#define MAINMENUPRESENTER_H

#include "Presenter.h"

class MainMenuView;

class MainMenuPresenter : public Presenter {
public:
    MainMenuPresenter();
    ~MainMenuPresenter();
    void setView(MainMenuView * view);
    void play() const;
    void training() const;
    void exit() const;

private:
    MainMenuView * _view;
};

#endif // MAINMENUPRESENTER_H
