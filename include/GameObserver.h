#ifndef GAMEOBSERVER_H
#define GAMEOBSERVER_H

#include <QObject>

class GamePresenter;

class GameObserver : public QObject {
    Q_OBJECT

public:
    GameObserver(GamePresenter *presenter, QObject * parent = nullptr);

public slots:
    void onGameCompletion();

private:
    GamePresenter * _presenter;
};

#endif // GAMEOBSERVER_H
