#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QWidget>

class TakuzuGame;
class GamePresenter;

namespace Takuzu {
enum PawnType {
    Circle,
    Square
};
}

class GameBoard : public QWidget
{
    Q_OBJECT
public:
    explicit GameBoard(QWidget *parent = nullptr);
    void setGame(TakuzuGame * game);
    void setPresenter(GamePresenter *);
    Takuzu::PawnType getPawnType();
protected:
    void paintEvent(QPaintEvent *event) override;
    void mouseReleaseEvent(QMouseEvent * event) override;
    void resizeEvent(QResizeEvent *event) override;
signals:
    void isRedrawn();
public slots:
    void changePawnType(QAction * action);
private:
    void drawCell(QPainter * painter, int x, int y, QRect position);
    void drawPawn(QPainter * painter,  QRect position);
    std::pair<int, int> getCellClicked(int x, int y);
private:
    Takuzu::PawnType _pawnType = Takuzu::Circle;
    TakuzuGame * _game;
    GamePresenter * _presenter;
    QRect _cellSize;
};

#endif // GAMEBOARD_H
