#ifndef LINECOUNTER_H
#define LINECOUNTER_H

#include <QWidget>

class TakuzuGame;
class GameBoard;

class LineCounter : public QWidget
{
    Q_OBJECT
public:
    explicit LineCounter(QWidget *parent = nullptr);
    void setGame(TakuzuGame * game);
    void setGameBoard(GameBoard * gameBoard);
protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
signals:

public slots:

private:
    TakuzuGame * _game;
    GameBoard * _gameBoard;
};

#endif // LINECOUNTER_H
