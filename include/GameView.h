#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include <QWidget>

namespace Ui {
class GameView;
}

class GameBoard;
class GamePresenter;
class LineCounter;
class ColumnCounter;

class GameView : public QWidget
{
    Q_OBJECT

public:
    explicit GameView(QWidget *parent = 0);
    ~GameView();
    void setPresenter(GamePresenter * presenter);
    void setTimerLabelText(const QString &);
    void setUndoCountLabelText(const QString &);
    void setUndo(bool);
    void setRedo(bool);
    QTimer * getTimer();
    GameBoard * getGameBoard();
    LineCounter * getLineCounter();
    ColumnCounter * getColumnCounter();

public slots:
    void onReturnPressed() const;
    void onTimeout();
    void onUndoPressed() const;
    void onRedoPressed() const;

private:
    Ui::GameView *ui;
    GamePresenter * _presenter;
    GameBoard * _gameBoard;
    QTimer * _timer;
};

#endif // GAMEVIEW_H
