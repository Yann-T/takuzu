#ifndef SQUAREWIDGET_H
#define SQUAREWIDGET_H

#include <QWidget>

class SquareWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SquareWidget(QWidget *parent = nullptr);
protected:
    void resizeEvent(QResizeEvent *event) override;
signals:

public slots:
};

#endif // SQUAREWIDGET_H
