#ifndef PRESENTER_H
#define PRESENTER_H

class ViewManager;

class Presenter
{
public:
    Presenter();
    virtual ~Presenter() {}
    virtual void initialize();

    void setViewManager(ViewManager * manager);
    ViewManager * getViewManager();

protected:
    ViewManager * _manager;
};

#endif // PRESENTER_H
