#ifndef GRIDSELECTIONVIEW_H
#define GRIDSELECTIONVIEW_H

#include <QWidget>

namespace Ui {
class GridSelectionView;
}

class GridSelectionPresenter;

class GridSelectionView : public QWidget
{
    Q_OBJECT

public:
    explicit GridSelectionView(QWidget *parent = 0);
    ~GridSelectionView();
    void setPresenter(GridSelectionPresenter * presenter);
    void setEnabledStartButton(bool) const;

public slots:
    void onReturnPressed() const;
    void onStartPressed() const;
    void onEasyChecked() const;
    void onHardChecked() const;
    void on6x6Checked() const;
    void on8x8Checked() const;
    void on10x10Checked() const;

private:
    Ui::GridSelectionView *ui;
    GridSelectionPresenter * _presenter;
};

#endif // GRIDSELECTIONVIEW_H
