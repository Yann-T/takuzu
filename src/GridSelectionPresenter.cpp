#include "include/GridSelectionPresenter.h"

#include "include/GridSelectionView.h"
#include "include/ViewManager.h"
#include "include/GamePresenter.h"
#include <QDebug>

GridSelectionPresenter::GridSelectionPresenter() : _difficulty(Takuzu::Difficulty::DifficultyNotSet), _size(Takuzu::Size::SizeNotSet)
{

}

GridSelectionPresenter::~GridSelectionPresenter() {
    qDebug() << "GridSelectionPresenter::~GridSelectionPresenter()";
}

void GridSelectionPresenter::setView(GridSelectionView * view) {
    _view = view;
    updateStartAccess();
}

void GridSelectionPresenter::setGamePresenter(GamePresenter * gamePresenter) {
    _gamePresenter = gamePresenter;
}

void GridSelectionPresenter::returnToMenu() const {
    qDebug() << "GridSelectionPresenter::returnToMenu()";
    _manager->changeView(Takuzu::MainMenu);
}

void GridSelectionPresenter::startGame() const {
    qDebug() << "GridSelectionPresenter::startGame()";
    std::map<QString, int> parameters;
    parameters.insert(std::pair<QString, int>("difficulty", _difficulty));
    parameters.insert(std::pair<QString, int>("size", _size));
    _manager->setParameter(parameters);
    _gamePresenter->initialize();
    _manager->changeView(Takuzu::Game);
}

void GridSelectionPresenter::setDifficulty(Takuzu::Difficulty difficulty) {
    qDebug() << "GridSelectionPresenter::setDifficulty(" << difficulty << ")";
    _difficulty = difficulty;
    updateStartAccess();
}

void GridSelectionPresenter::setSize(Takuzu::Size size) {
    qDebug() << "GridSelectionPresenter::setSize(" << size << ")";
    _size = size;
    updateStartAccess();
}

void GridSelectionPresenter::updateStartAccess() const {
    _view->setEnabledStartButton(parametersAreSet());
}

bool GridSelectionPresenter::parametersAreSet() const {
    return (_difficulty != Takuzu::Difficulty::DifficultyNotSet) && (_size != Takuzu::Size::SizeNotSet);
}
