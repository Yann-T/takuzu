#include "include/GameObserver.h"
#include "include/GamePresenter.h"
#include <QDebug>


GameObserver::GameObserver(GamePresenter *presenter, QObject *parent) : _presenter(presenter), QObject(parent) {}

void GameObserver::onGameCompletion() {
    _presenter->stopTimer();
}
