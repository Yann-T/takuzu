#include "include/MainMenuView.h"
#include "ui_MainMenuView.h"
#include <QWidget>

#include "include/MainMenuPresenter.h"

MainMenuView::MainMenuView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenuView)
{
    ui->setupUi(this);
    connect(ui->playPushButton, SIGNAL(clicked(bool)), this, SLOT(onPlayClick()));
    connect(ui->trainingPushButton, SIGNAL(clicked(bool)), this, SLOT(onTrainingClick()));
    connect(ui->exitPushButton, SIGNAL(clicked(bool)), this, SLOT(onExitClick()));
}

MainMenuView::~MainMenuView()
{
    delete _presenter;
    delete ui;
}

void MainMenuView::setPresenter(MainMenuPresenter *presenter) {
    _presenter = presenter;
}

void MainMenuView::onPlayClick() const {
    _presenter->play();
}

void MainMenuView::onTrainingClick() const {
    _presenter->training();
}

void MainMenuView::onExitClick() const {
    _presenter->exit();
}
