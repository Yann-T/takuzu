#include "include/MainMenuPresenter.h"

#include "include/MainMenuView.h"
#include "include/ViewManager.h"

#include <QDebug>

MainMenuPresenter::MainMenuPresenter()
{

}

MainMenuPresenter::~MainMenuPresenter() {
    qDebug() << "MainMenuPresenter::~MainMenuPresenter()";
}

void MainMenuPresenter::setView(MainMenuView *view) {
    _view = view;
}

void MainMenuPresenter::play() const {
    qDebug() << "MainMenuPresenter::play()";
    _manager->changeView(Takuzu::GridSelection);
}

void MainMenuPresenter::training() const {
    qDebug() << "MainMenuPresenter::training()";
}

void MainMenuPresenter::exit() const {
    qDebug() << "MainMenuPresenter::exit()";
    _manager->exit();
}
