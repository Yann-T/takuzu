#include "include/GamePresenter.h"
#include "include/GameView.h"
#include "include/ViewManager.h"
#include "include/GameBoard.h"
#include "include/LineCounter.h"
#include "include/ColumnCounter.h"
#include "include/GameObserver.h"
#include "include/Game/TakuzuGame.h"
#include "include/Command/CommandStack.h"
#include "include/Command/NextPawn.h"
#include "include/Command/PreviousPawn.h"
#include <QDebug>
#include <QTime>
#include <QTimer>

GamePresenter::GamePresenter()
{

}

GamePresenter::~GamePresenter()
{

}

void GamePresenter::initialize() {
    auto parameters = _manager->getParameter();
    _game = new TakuzuGame((Takuzu::Size)parameters["size"], (Takuzu::Difficulty)parameters["difficulty"]);
    GameObserver * observer = new GameObserver(this, _game);
    _game->connect(_game, SIGNAL(completed()), observer, SLOT(onGameCompletion()));
    _time = QTime(0,0);
    _view->getGameBoard()->setGame(_game);
    _view->getLineCounter()->setGame(_game);
    _view->getLineCounter()->setGameBoard(_view->getGameBoard());
    _view->getColumnCounter()->setGame(_game);
    _view->getColumnCounter()->setGameBoard(_view->getGameBoard());
    _view->getTimer()->start(1000);
    _stack = CommandStack();
    _undoCount = 0;
    updateUndoCount();
    updateUndoRedoState();
}

void GamePresenter::returnToSelection() const
{
    qDebug() << "GamePresenter::returnToSelection()";
    _view->getTimer()->stop();
    _manager->changeView(Takuzu::GridSelection);
}

void GamePresenter::updateTimer() {
    _time = _time.addMSecs(1000);
    _view->setTimerLabelText(_time.toString("mm:ss"));
}

void GamePresenter::updateUndoRedoState() {
    _view->setUndo(_stack.canUndo());
    _view->setRedo(_stack.canRedo());
}

void GamePresenter::updateUndoCount() {
    _view->setUndoCountLabelText(QString("Undo: %1").arg(_undoCount));
}

void GamePresenter::stopTimer() {
    _view->getTimer()->stop();
}

void GamePresenter::setView(GameView *view)
{
    _view = view;
}

TakuzuGame * GamePresenter::getGame() {
    return _game;
}

void GamePresenter::onRightClick(int y, int x) {
    if (!_game->isLockedCell(y,x)) {
        NextPawn * command = new NextPawn(_game, y, x);
        _stack.push(command);
        command->execute();
        updateUndoRedoState();
    }
}

void GamePresenter::onLeftClick(int y, int x) {
    if (!_game->isLockedCell(y,x)) {
        PreviousPawn * command = new PreviousPawn(_game, y, x);
        _stack.push(command);
        command->execute();
        updateUndoRedoState();
    }
}

void GamePresenter::undo() {
    _stack.undoLastAction();
    _undoCount++;
    updateUndoRedoState();
    updateUndoCount();
}

void GamePresenter::redo() {
    _stack.redoLastUndoneAction();
    updateUndoRedoState();
}
