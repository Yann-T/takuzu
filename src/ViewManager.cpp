#include "include/ViewManager.h"
#include "include/MainWindow.h"
#include "include/MainMenuView.h"
#include "include/MainMenuPresenter.h"
#include "include/GridSelectionPresenter.h"
#include "include/GridSelectionView.h"
#include <QStackedWidget>
#include <QDebug>
#include <map>

ViewManager::ViewManager(MainWindow * window, QStackedWidget * stack)
{
    _window = window;
    _stack = stack;
}

ViewManager::~ViewManager() {
    qDebug() << "ViewManager::~ViewManager()";
}

void ViewManager::changeView(Takuzu::View newView) {
    qDebug() << "ViewManager::changeView()";
    _stack->setCurrentIndex(newView);
}

void ViewManager::exit() {
    qDebug()<< "ViewManager::exit()";
    _window->close();
}

void ViewManager::setParameter(std::map<QString, int> parameters) {
    _parameters = parameters;
}

std::map<QString, int> ViewManager::getParameter() const {
    return _parameters;
}
