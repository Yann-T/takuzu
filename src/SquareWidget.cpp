#include "include/SquareWidget.h"

SquareWidget::SquareWidget(QWidget *parent) : QWidget(parent)
{

}

void SquareWidget::resizeEvent(QResizeEvent * /*event*/) {
    QSize s = size();
    if(s.height() < s.width()) {
        resize(s.height(), s.height());
    } else {
        resize(s.width(), s.width());
    }
}
