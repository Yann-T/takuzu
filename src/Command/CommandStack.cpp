#include "include/Command/CommandStack.h"
#include "include/Command/Command.h"
#include <deque>

CommandStack::CommandStack() : _actionStack(), _undoneActionStack() {}

void CommandStack::push(Command * c) {
    _actionStack.push_back(c);
    _undoneActionStack.clear();
}

bool CommandStack::canUndo() const {
    return !_actionStack.empty();
}

bool CommandStack::canRedo() const {
    return !_undoneActionStack.empty();
}

void CommandStack::undoLastAction() {
    if (canUndo()) {
        Command * lastAction = _actionStack.back();
        _undoneActionStack.push_back(lastAction);
        _actionStack.pop_back();
        lastAction->undo();
    }
}

void CommandStack::redoLastUndoneAction() {
    if (canRedo()) {
        Command * lastUndoneAction = _undoneActionStack.back();
        _actionStack.push_back(lastUndoneAction);
        _undoneActionStack.pop_back();
        lastUndoneAction->redo();
    }
}
