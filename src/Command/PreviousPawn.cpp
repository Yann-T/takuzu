#include "include/Command/PreviousPawn.h"
#include "include/Command/TakuzuGameCommand.h"
#include "include/Game/TakuzuGame.h"

PreviousPawn::PreviousPawn(TakuzuGame * game, unsigned int row, unsigned int column) : TakuzuGameCommand(game), _row(row), _column(column) {}

void PreviousPawn::execute() {
    _game->setPreviousPawnAt(_row, _column);
}

void PreviousPawn::undo() {
    _game->setNextPawnAt(_row, _column);
}

void PreviousPawn::redo() {
    _game->setPreviousPawnAt(_row, _column);
}
