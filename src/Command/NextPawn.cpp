#include "include/Command/NextPawn.h"
#include "include/Command/TakuzuGameCommand.h"
#include "include/Game/TakuzuGame.h"

NextPawn::NextPawn(TakuzuGame * game, unsigned int row, unsigned int column) : TakuzuGameCommand(game), _row(row), _column(column) {}

void NextPawn::execute() {
    _game->setNextPawnAt(_row, _column);
}

void NextPawn::undo() {
    _game->setPreviousPawnAt(_row, _column);
}

void NextPawn::redo() {
    _game->setNextPawnAt(_row, _column);
}
