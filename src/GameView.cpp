#include "include/GameView.h"
#include "ui_GameView.h"
#include "include/GamePresenter.h"
#include <QDebug>
#include <QTimer>

GameView::GameView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameView)
{
    _timer = new QTimer(this);
    ui->setupUi(this);
    connect(ui->BackButton, SIGNAL(clicked(bool)), this, SLOT(onReturnPressed()));
    connect(ui->gameBoardWidget, SIGNAL(isRedrawn()), ui->lineCounterWidget, SLOT(repaint()));
    connect(ui->gameBoardWidget, SIGNAL(isRedrawn()), ui->columnCounterWidget, SLOT(repaint()));
    connect(ui->undoPushButton, SIGNAL(clicked(bool)), this, SLOT(onUndoPressed()));
    connect(ui->redoPushButton, SIGNAL(clicked(bool)), this, SLOT(onRedoPressed()));
    connect(ui->undoPushButton, SIGNAL(clicked(bool)), ui->gameBoardWidget, SLOT(repaint()));
    connect(ui->redoPushButton, SIGNAL(clicked(bool)), ui->gameBoardWidget, SLOT(repaint()));
    connect(_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
}

GameView::~GameView()
{
    delete ui;
    delete _presenter;
}

void GameView::setPresenter(GamePresenter * presenter) {
    _presenter = presenter;
    ui->gameBoardWidget->setPresenter(presenter);
}

void GameView::setTimerLabelText(const QString & s) {
    ui->timerLabel->setText(s);
}

void GameView::setUndoCountLabelText(const QString & s) {
    ui->undoLabel->setText(s);
}

QTimer * GameView::getTimer() {
    return _timer;
}

GameBoard * GameView::getGameBoard() {
    return ui->gameBoardWidget;
}

LineCounter * GameView::getLineCounter() {
    return ui->lineCounterWidget;
}

ColumnCounter * GameView::getColumnCounter() {
    return ui->columnCounterWidget;
}

void GameView::setUndo(bool enable) {
    ui->undoPushButton->setEnabled(enable);
}

void GameView::setRedo(bool enable) {
    ui->redoPushButton->setEnabled(enable);
}

void GameView::onReturnPressed() const {
    qDebug() << "GameView::onReturnPressed()";
    _presenter->returnToSelection();
}

void GameView::onTimeout() {
    _presenter->updateTimer();
}

void GameView::onUndoPressed() const {
    _presenter->undo();
}

void GameView::onRedoPressed() const {
    _presenter->redo();
}
