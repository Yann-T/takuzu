#include "include/GridSelectionView.h"
#include "ui_GridSelectionView.h"
#include "include/GridSelectionPresenter.h"

GridSelectionView::GridSelectionView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GridSelectionView)
{
    ui->setupUi(this);
    connect(ui->returnPushButton, SIGNAL(clicked(bool)), this, SLOT(onReturnPressed()));
    connect(ui->startPushButton, SIGNAL(clicked(bool)), this, SLOT(onStartPressed()));
    connect(ui->easyRadioButton, SIGNAL(clicked(bool)), this, SLOT(onEasyChecked()));
    connect(ui->hardRadioButton, SIGNAL(clicked(bool)), this, SLOT(onHardChecked()));
    connect(ui->sixRadioButton, SIGNAL(clicked(bool)), this, SLOT(on6x6Checked()));
    connect(ui->eightRadioButton, SIGNAL(clicked(bool)), this, SLOT(on8x8Checked()));
    connect(ui->tenRadioButton, SIGNAL(clicked(bool)), this, SLOT(on10x10Checked()));
}

GridSelectionView::~GridSelectionView()
{
    delete _presenter;
    delete ui;
}

void GridSelectionView::setPresenter(GridSelectionPresenter * presenter) {
    _presenter = presenter;
}

void GridSelectionView::onReturnPressed() const {
    _presenter->returnToMenu();
}

void GridSelectionView::onStartPressed() const {
    _presenter->startGame();
}

void GridSelectionView::onEasyChecked() const {
    _presenter->setDifficulty(Takuzu::Easy);
}

void GridSelectionView::onHardChecked() const {
    _presenter->setDifficulty(Takuzu::Hard);
}

void GridSelectionView::on6x6Checked() const {
    _presenter->setSize(Takuzu::Six);
}

void GridSelectionView::on8x8Checked() const {
    _presenter->setSize(Takuzu::Eight);
}

void GridSelectionView::on10x10Checked() const {
    _presenter->setSize(Takuzu::Ten);
}

void GridSelectionView::setEnabledStartButton(bool state) const {
    ui->startPushButton->setEnabled(state);
}
