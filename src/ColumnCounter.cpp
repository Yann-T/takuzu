#include "include/ColumnCounter.h"
#include "include/GameBoard.h"
#include "include/Game/TakuzuGame.h"
#include <QPainter>
#include <QDebug>

ColumnCounter::ColumnCounter(QWidget *parent) : QWidget(parent)
{

}

void ColumnCounter::setGame(TakuzuGame * game) {
    _game = game;
}

void ColumnCounter::setGameBoard(GameBoard * gameBoard) {
    _gameBoard = gameBoard;
}

void ColumnCounter::paintEvent(QPaintEvent * /*event*/) {
    QPainter * painter = new QPainter(this);
    painter->eraseRect(rect());

    if(_game == nullptr) {
        return;
    }

    unsigned short size;
    switch(_game->getSize()) {
        case Takuzu::SizeNotSet : return;
        case Takuzu::Six : size = 6; break;
        case Takuzu::Eight : size = 8; break;
        case Takuzu::Ten : size = 10; break;
    }

    float widgetWidth = rect().width();
    float cellWidth = widgetWidth / size;

    painter->setPen(QPen(QColor(0, 0, 0)));
    for(int i = 0; i < size; i++) {
        std::pair<int, int> pawns =_game->countPawnsInColumn(i);
        if(pawns.first == size / 2 && pawns.first == pawns.second) {
            QImage img(":resources/Images/check.png");
            painter->drawImage(QRectF(i * cellWidth + cellWidth/4, cellWidth/2, cellWidth/2, cellWidth/2),img);
        } else {
            QFont font = painter->font();
            int fontsize;
            if(size == 6) {
                fontsize = 22;
            } else if(size == 8) {
                fontsize = 18;
            } else if(size == 10) {
                fontsize = 16;
            }
            font.setPointSize(fontsize);
            painter->setFont(font);
            painter->drawText(QRectF(i * cellWidth + cellWidth/3, fontsize , cellWidth / 2, 100), QString("%1").arg(pawns.second));
            painter->setPen(QPen(QBrush(QColor(0, 0, 0)), 2));
            painter->drawLine(i * cellWidth + cellWidth/4, fontsize*2.8, i * cellWidth + cellWidth - cellWidth/3, fontsize*2.8);
            painter->drawText(QRectF(i * cellWidth + cellWidth/3, fontsize * 3 , cellWidth / 2, 100),  QString("%1").arg(pawns.first));
        }
    }
    painter->end();
}

void ColumnCounter::resizeEvent(QResizeEvent * /*event*/) {;
    resize(_gameBoard->width(), 100);
}
