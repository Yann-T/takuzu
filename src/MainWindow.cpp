#include "include/MainWindow.h"
#include "ui_MainWindow.h"

#include "include/MainMenuPresenter.h"
#include "include/GridSelectionPresenter.h"
#include "include/GamePresenter.h"
#include "include/ViewManager.h"
#include "include/Game/TakuzuGridLoader.h"
#include "include/GameBoard.h"
#include <QStackedWidget>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _manager = new ViewManager(this, ui->stackedWidget);

    MainMenuPresenter * mainMenuPresenter = new MainMenuPresenter();
    mainMenuPresenter->setViewManager(_manager);
    mainMenuPresenter->setView(ui->mainMenu);
    ui->mainMenu->setPresenter(mainMenuPresenter);

    GridSelectionPresenter * gridSelectionPresenter = new GridSelectionPresenter();
    gridSelectionPresenter->setViewManager(_manager);
    gridSelectionPresenter->setView(ui->gridSelection);
    ui->gridSelection->setPresenter(gridSelectionPresenter);

    GamePresenter * gamePresenter = new GamePresenter();
    gamePresenter->setViewManager(_manager);
    gamePresenter->setView(ui->game);
    ui->game->setPresenter(gamePresenter);
    gridSelectionPresenter->setGamePresenter(gamePresenter);

    QMenu * optionMenu = menuBar()->addMenu("&Options");
    QMenu * pawnMenu = optionMenu->addMenu("&Pawn style");
    QActionGroup * pawnActionGroup = new QActionGroup(this);
    pawnActionGroup->setExclusive(true);


    QAction * action = pawnMenu->addAction("&Circle");
    action->setData(Takuzu::Circle);
    action->setCheckable(true);
    action->setChecked(true);
    pawnActionGroup->addAction(action);
    action = pawnMenu->addAction("&Square");
    action->setData(Takuzu::Square);
    action->setCheckable(true);
    pawnActionGroup->addAction(action);
    connect(pawnActionGroup, SIGNAL(triggered(QAction*)), ui->game->getGameBoard() , SLOT(changePawnType(QAction*)));
    this->menuBar()->addMenu(optionMenu);
    _manager->changeView(Takuzu::MainMenu);
}

MainWindow::~MainWindow()
{
    delete _manager;
    delete ui;
}
