#include "include/LineCounter.h"
#include "include/GameBoard.h"
#include "include/Game/TakuzuGame.h"
#include <QPainter>
#include <QDebug>
#include <QFont>
#include <QImage>

LineCounter::LineCounter(QWidget *parent) : QWidget(parent)
{

}

void LineCounter::setGame(TakuzuGame * game) {
    _game = game;
}

void LineCounter::setGameBoard(GameBoard * gameBoard) {
    _gameBoard = gameBoard;
}

void LineCounter::paintEvent(QPaintEvent * /*event*/) {
    QPainter * painter = new QPainter(this);
    painter->eraseRect(rect());

    if(_game == nullptr) {
        return;
    }

    unsigned short size;
    switch(_game->getSize()) {
        case Takuzu::SizeNotSet : return;
        case Takuzu::Six : size = 6; break;
        case Takuzu::Eight : size = 8; break;
        case Takuzu::Ten : size = 10; break;
    }

    float widgetHeight = rect().height();
    float cellHeight = widgetHeight / size;

    painter->setPen(QPen(QColor(0, 0, 0)));
    for(int i = 0; i < size; i++) {
        std::pair<int, int> pawns =_game->countPawnsInRow(i);
        if(pawns.first == size / 2 && pawns.first == pawns.second) {
            QImage img(":resources/Images/check.png");
            painter->drawImage(QRectF(60 - cellHeight/4, i * cellHeight + cellHeight/4, cellHeight/2, cellHeight/2),img);
        } else {
            QString string = QString("%1 | %2").arg(pawns.second).arg(pawns.first);

            QFont font = painter->font();
            if(size == 6) {
                font.setPointSize(22);
            } else if(size == 8) {
                font.setPointSize(18);
            } else if(size == 10) {
                font.setPointSize(16);
            }
            painter->setFont(font);
            painter->drawText(QRectF(50 - cellHeight/4, i * cellHeight + cellHeight/4, 100, cellHeight / 2), string);
        }
    }

    painter->end();
}

void LineCounter::resizeEvent(QResizeEvent * /*event*/) {
    resize(100, _gameBoard->height());
}
