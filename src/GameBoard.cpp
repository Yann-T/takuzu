#include "include/GameBoard.h"
#include "include/Game/TakuzuGame.h"
#include "include/GamePresenter.h"
#include "include/MainWindow.h"

#include <QPainter>
#include <QBrush>
#include <QColor>
#include <QDebug>
#include <QMouseEvent>
#include <cmath>
#include <QAction>

GameBoard::GameBoard(QWidget *parent) : QWidget(parent)
{
}

void GameBoard::paintEvent(QPaintEvent * /*event*/) {
    QPainter * painter = new QPainter(this);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->fillRect(rect(), QBrush(QColor(255, 255, 255)));

    if(_game == nullptr) {
        return;
    }

    unsigned short size;
    switch(_game->getSize()) {
        case Takuzu::SizeNotSet : return;
        case Takuzu::Six : size = 6; break;
        case Takuzu::Eight : size = 8; break;
        case Takuzu::Ten : size = 10; break;
    }

    float widgetWidth = rect().width();
    float widgetHeight = rect().height();
    float cellWidth = widgetWidth / size;
    float cellHeight = widgetHeight / size;

    _cellSize = QRect(0, 0, cellWidth, cellHeight);

    painter->setPen(QPen(QColor(0, 0, 0)));
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            QRect cell(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
            drawCell(painter, j, i, cell);
        }
    }

    painter->end();
    emit isRedrawn();
}

void GameBoard::drawCell(QPainter * painter, int x, int y, QRect position) {
    TakuzuGrid * grid = _game->getGrid();
    const Takuzu::PawnsSet & illegalPawns = _game->getIllegalPawns();
    painter->drawRect(position);

    int offset = 4;

    position.setWidth(position.width() - offset);
    position.setHeight(position.height() - offset);
    position.setX(position.x() + offset);
    position.setY(position.y() + offset);

    if (illegalPawns.find(std::make_pair<unsigned int, unsigned int>(x, y)) != illegalPawns.end()) {
        painter->setPen(QPen(Qt::red, 2));
    } else if (_game->isComplete()) {
        painter->setPen(QPen(Qt::green, 2));
    }

    switch(grid->get(x,y)) {
        case Takuzu::LockedBlack :
            painter->setBrush(QBrush(QColor(20, 20, 20)));
            drawPawn(painter, position);
            painter->setBrush(QBrush(QColor(245, 245, 245)));
            painter->drawEllipse(position.center().x() - 1, position.center().y() - 1, 4, 4);
            break;
        case Takuzu::Black :
            painter->setBrush(QBrush(QColor(20, 20, 20)));
            drawPawn(painter, position);
            break;
        case Takuzu::LockedWhite :
            painter->setBrush(QBrush(QColor(245, 245, 245)));
            drawPawn(painter, position);
            painter->setBrush(QBrush(QColor(20, 20, 20)));
            painter->drawEllipse(position.center().x() - 1, position.center().y() - 1, 4, 4);
            break;
        case Takuzu::White :
            painter->setBrush(QBrush(QColor(245, 245, 245)));
            drawPawn(painter, position);
            break;
        default:
            break;
    }

    painter->setBrush(QBrush(QColor(255, 255, 255)));
    painter->setPen(Qt::black);
}

void GameBoard::drawPawn(QPainter * painter,  QRect position) {
    switch(getPawnType()) {
        case Takuzu::Circle :
            painter->drawEllipse(QRectF(position));
            break;
        case Takuzu::Square :
            painter->drawRect(position);
            break;
    }
}

void GameBoard::setGame(TakuzuGame * game) {
    _game = game;
}

void GameBoard::setPresenter(GamePresenter * presenter) {
    _presenter = presenter;
}

std::pair<int, int> GameBoard::getCellClicked(int x, int y) {
    int x_grid = floor(x / _cellSize.width());
    int y_grid = floor(y / _cellSize.height());
    int size;
    switch(_game->getSize()) {
        case Takuzu::Six : size = 6; break;
        case Takuzu::Eight : size = 8; break;
        case Takuzu::Ten : size = 10; break;
    }
    if(x_grid > size - 1) {
        x_grid = size - 1;
    }
    if(y_grid > size - 1) {
        y_grid = size - 1;
    }
    return std::make_pair(x_grid,y_grid);
}

void GameBoard::mouseReleaseEvent(QMouseEvent * event) {
    std::pair<int, int> pos = getCellClicked(event->x(), event->y());
    if(event->button() == Qt::LeftButton) {
        _presenter->onLeftClick(pos.second, pos.first);
    } else if(event->button() == Qt::RightButton) {
        _presenter->onRightClick(pos.second, pos.first);
    }
    this->repaint();
}

void GameBoard::resizeEvent(QResizeEvent * /*event*/) {
    QSize s = size();
    if(s.height() < s.width()) {
        resize(s.height(), s.height());
    } else {
        resize(s.width(), s.width());
    }
}

Takuzu::PawnType GameBoard::getPawnType() {
    return _pawnType;
}

void GameBoard::changePawnType(QAction * action) {
   _pawnType = (Takuzu::PawnType)action->data().toInt();
   this->repaint();
}
