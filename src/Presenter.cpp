#include "include/Presenter.h"
#include "include/ViewManager.h"

Presenter::Presenter()
{

}

void Presenter::initialize() {}

void Presenter::setViewManager(ViewManager * manager) {
    _manager = manager;
}

ViewManager * Presenter::getViewManager() {
    return _manager;
}
