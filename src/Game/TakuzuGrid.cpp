#include "include/Game/TakuzuGrid.h"
#include <cmath>
#include <QDebug>

TakuzuGrid::TakuzuGrid(unsigned short size): _size(size), _grid(size*size) {}

const Takuzu::Pawn & TakuzuGrid::get(unsigned int row, unsigned int column) const {
    return _grid.at(row*_size + column%_size);
}

const Takuzu::Pawn & TakuzuGrid::operator()(unsigned int row, unsigned int column) const {
    return get(row, column);
}

Takuzu::Pawn & TakuzuGrid::get(unsigned int row, unsigned int column) {
    return _grid.at(row*_size + column%_size);
}

Takuzu::Pawn & TakuzuGrid::operator()(unsigned int row, unsigned int column) {
    return get(row, column);
}

unsigned short TakuzuGrid::size() const { return _size; }

bool TakuzuGrid::notOnBorder(unsigned int i) const { return ((unsigned int)0 < i) && (i < (unsigned int)(_size-1)); }

TakuzuGrid TakuzuGrid::stringToGrid(const std::string & string) {
    qDebug() << "DEBUG: Generating: " << string.data();
    TakuzuGrid grid(sqrt(string.size()));
    auto it = string.begin();
    for(Takuzu::Pawn & pawn : grid._grid) {
        pawn = TakuzuGrid::charToPawn(*(it++));
    }
    return grid;
}

Takuzu::Pawn TakuzuGrid::charToPawn(char c) {
    switch(c) {
    case 'W':
        return Takuzu::LockedWhite;
    case 'B':
        return Takuzu::LockedBlack;
    case '.':
        return Takuzu::Empty;
    default:
        throw "Character not recognized";
    }
}

char TakuzuGrid::pawnToChar(Takuzu::Pawn p) {
    switch(p) {
    case Takuzu::White:
        return 'W';
    case Takuzu::LockedWhite:
        return 'W';
    case Takuzu::Black:
        return 'B';
    case Takuzu::LockedBlack:
        return 'B';
    case Takuzu::Empty:
        return '.';
    default:
        throw "Not a supported pawn.";
    }
}

QDebug operator<<(QDebug os, const TakuzuGrid & grid) {
    for(unsigned int row=0; row < grid.size(); row++) {
        for(unsigned int column=0; column < grid.size(); column++) {
            os.space() <<TakuzuGrid::pawnToChar(grid(row, column));
        }
        os.nospace() << '\n';
    }
    return os;
}
