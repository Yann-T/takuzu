#include "include/Game/TakuzuGridLoader.h"
#include "include/Game/TakuzuGrid.h"
#include "include/GridSelectionPresenter.h"
#include <QFile>
#include <QDebug>
#include <set>
#include <vector>
#include <chrono>
#include <random>

std::set<QString> TakuzuGridLoader::Easy = {":/resources/TakuzuGrids/6_easy.txt", ":/resources/TakuzuGrids/8_easy.txt", ":/resources/TakuzuGrids/10_easy.txt"};
std::set<QString> TakuzuGridLoader::Hard = {":/resources/TakuzuGrids/6_hard.txt", ":/resources/TakuzuGrids/8_hard.txt", ":/resources/TakuzuGrids/10_hard.txt"};
std::set<QString> TakuzuGridLoader::Six = {":/resources/TakuzuGrids/6_easy.txt", ":/resources/TakuzuGrids/6_hard.txt"};
std::set<QString> TakuzuGridLoader::Eight = {":/resources/TakuzuGrids/8_easy.txt", ":/resources/TakuzuGrids/8_hard.txt"};
std::set<QString> TakuzuGridLoader::Ten = {":/resources/TakuzuGrids/10_easy.txt", ":/resources/TakuzuGrids/10_hard.txt"};

TakuzuGridLoader::TakuzuGridLoader(const QString & path) : _gridPoolCount(0), _gridPoolStrings() {
    _gridPoolFile = new QFile(path);
    if (_gridPoolFile->open(QIODevice::ReadOnly)) {
        char buffer[64];
        _gridPoolFile->readLine(buffer, sizeof(buffer));
        _gridPoolCount = std::stoi(buffer);
        while(!_gridPoolFile->atEnd()) {
            char gridString[1024];
            _gridPoolFile->readLine(gridString, sizeof(gridString));
            _gridPoolStrings.emplace_back(gridString);
        }
        qDebug() << "DEBUG: Grids successfully loaded: Count =" << _gridPoolCount << "( Loaded:" << _gridPoolStrings.size() << ")";
        _gridPoolFile->close();
    }
}

TakuzuGridLoader::~TakuzuGridLoader() {
    qDebug() << "TakuzuGridLoader::~TakuzuGridLoader()";
    delete _gridPoolFile;
}

TakuzuGrid TakuzuGridLoader::getGridAt(unsigned int index) const {
    qDebug() << "DEBUG: Playing grid" << index;
    return TakuzuGrid::stringToGrid(_gridPoolStrings.at(index));
}

TakuzuGrid TakuzuGridLoader::getRandomGrid() const {
    std::minstd_rand0 generator(std::chrono::system_clock::now().time_since_epoch().count());
    return getGridAt(generator() % _gridPoolCount);
}

const TakuzuGridLoader & TakuzuGridLoader::getLoaderFromParameter(Takuzu::Difficulty difficulty, Takuzu::Size size) {
    std::set<QString> difficultyGrids = getGrids(difficulty);
    std::set<QString> sizeGrids = getGrids(size);
    QString result;
    for(QString s : difficultyGrids) {
        if (sizeGrids.find(s) != sizeGrids.end()) {
            result = s;
            qDebug() << result;
        }
    }
    return *(new TakuzuGridLoader(result));
}

std::set<QString> TakuzuGridLoader::getGrids(Takuzu::Difficulty difficulty) {
    switch(difficulty) {
    case Takuzu::Easy:
        return TakuzuGridLoader::Easy;
    case Takuzu::Hard:
        return TakuzuGridLoader::Hard;
    default:
        return std::set<QString>();
    }
}

std::set<QString> TakuzuGridLoader::getGrids(Takuzu::Size size) {
    switch(size) {
    case Takuzu::Six:
        return TakuzuGridLoader::Six;
    case Takuzu::Eight:
        return TakuzuGridLoader::Eight;
    case Takuzu::Ten:
        return TakuzuGridLoader::Ten;
    default:
        return std::set<QString>();
    }
}
