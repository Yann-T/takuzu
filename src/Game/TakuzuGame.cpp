#include "include/Game/TakuzuGame.h"
#include "include/Game/TakuzuGridLoader.h"

TakuzuGame::TakuzuGame(Takuzu::Size boardSize, Takuzu::Difficulty boardDifficulty, QObject *parent) : QObject(parent), _size(boardSize), _difficulty(boardDifficulty), _grid(0)
{
    TakuzuGridLoader gridLoader = TakuzuGridLoader::getLoaderFromParameter(boardDifficulty, boardSize);
    _grid = gridLoader.getRandomGrid();
    qDebug() << _grid;
}

void TakuzuGame::setPawnAt(Takuzu::Pawn pawn, unsigned int y, unsigned int x) {
    _grid(y, x) = pawn;
    if (isComplete()) {
        emit completed();
    }
}

void TakuzuGame::setNextPawnAt(unsigned int y, unsigned int x) {
    if (_grid(y, x) != Takuzu::LockedWhite && _grid(y, x) != Takuzu::LockedBlack) {
        Takuzu::Pawn nextPawn = (Takuzu::Pawn)((_grid(y, x) + 1)%3);
        setPawnAt(nextPawn, y, x);
    }
}

void TakuzuGame::setPreviousPawnAt(unsigned int y, unsigned int x) {
    if (_grid(y, x) != Takuzu::LockedWhite && _grid(y, x) != Takuzu::LockedBlack) {
        Takuzu::Pawn previousPawn = (Takuzu::Pawn)(_grid(y, x) - 1);
        if(previousPawn == -1) {
            previousPawn = (Takuzu::Pawn)2;
        }
        setPawnAt(previousPawn, y, x);
    }
}

std::pair<int, int> TakuzuGame::countPawnsInRow(unsigned int row) const {
    int black = 0;
    int white = 0;
    for(unsigned int column = 0; column < _grid.size(); column++) {
        if (_grid(row, column) == Takuzu::Black || _grid(row, column) == Takuzu::LockedBlack) {
            black++;
        } else if (_grid(row, column) == Takuzu::White || _grid(row, column) == Takuzu::LockedWhite) {
            white++;
        }
    }
    return std::make_pair(black, white);
}

std::pair<int, int> TakuzuGame::countPawnsInColumn(unsigned int column) const {
    int black = 0;
    int white = 0;
    for(unsigned int row = 0; row < _grid.size(); row++) {
        if (isBlack(_grid(row, column))) {
            black++;
        } else if (isWhite(_grid(row, column))) {
            white++;
        }
    }
    return std::make_pair(black, white);
}

Takuzu::PawnsSet TakuzuGame::getIllegalPawns() const {
    Takuzu::PawnsSet illegalPawns;
    insertIllegalPawnsOnRows(illegalPawns);
    insertIllegalPawnsOnColumns(illegalPawns);
    insertIllegalPawnsBetweenTwoPawnsOfTheSameColor(illegalPawns);
    insertIllegalPawnsInSimilarRows(illegalPawns);
    insertIllegalPawnsInSimilarColumns(illegalPawns);
    return illegalPawns;
}

Takuzu::Size TakuzuGame::getSize() const {
    return _size;
}

TakuzuGrid * TakuzuGame::getGrid() {
    return &_grid;
}
bool TakuzuGame::isBalanced(std::pair<int, int> counts) const {
    return counts.first == _grid.size()/2 && counts.second == _grid.size()/2;
}

bool TakuzuGame::isComplete() const {
    for(int i = 0; i < _grid.size(); i++) {
        std::pair<int, int> countInRow = countPawnsInColumn(i);
        std::pair<int, int> countInColumn = countPawnsInColumn(i);
        if (!isBalanced(countInRow) || !isBalanced(countInColumn)) {
            return false;
        }
    }
    return getIllegalPawns().empty();
}

bool TakuzuGame::isLockedCell(unsigned int row, unsigned column) {
    return _grid(row, column) == Takuzu::LockedBlack || _grid(row, column) == Takuzu::LockedWhite;
}

bool TakuzuGame::isWhite(Takuzu::Pawn pawn) const {
    return (pawn == Takuzu::White) || (pawn == Takuzu::LockedWhite);
}

bool TakuzuGame::isBlack(Takuzu::Pawn pawn) const {
    return (pawn == Takuzu::Black) || (pawn == Takuzu::LockedBlack);
}

bool TakuzuGame::areSameColor(Takuzu::Pawn p0, Takuzu::Pawn p1) const {
    if (isBlack(p0)) {
        return isBlack(p1);
    } else if (isWhite(p0)) {
        return isWhite(p1);
    } else {
        return p0 == p1;
    }
}

bool TakuzuGame::isBetweenTwoPawnsOfTheSameColorInRow(unsigned int y, unsigned int x) const {
    Takuzu::Pawn pawn = _grid(y,x);
    return areSameColor(pawn, _grid(y, x-1)) && areSameColor(pawn, _grid(y, x+1));

}

bool TakuzuGame::isBetweenTwoPawnsOfTheSameColorInColumn(unsigned int y, unsigned int x) const {
    Takuzu::Pawn pawn = _grid(y,x);
    return areSameColor(pawn, _grid(y-1, x)) && areSameColor(pawn, _grid(y+1, x));
}

bool TakuzuGame::areSameRow(unsigned int y1, unsigned int y2) const {
    for(unsigned int column = 0; column < _grid.size(); column++) {
        if (!areSameColor(_grid(y1, column), _grid(y2, column)) || (_grid(y1, column) == Takuzu::Empty)) {
            return false;
        }
    }
    return true;
}

bool TakuzuGame::areSameColumn(unsigned int x1, unsigned int x2) const {
    for(unsigned int row = 0; row < _grid.size(); row++) {
        if (!areSameColor(_grid(row, x1),_grid(row, x2)) || (_grid(row, x1) == Takuzu::Empty)) {
            return false;
        }
    }
    return true;
}

void TakuzuGame::insertIllegalPawnsOnRows(Takuzu::PawnsSet & set) const {
    std::pair<int, int> bwCount;
    for (unsigned int row = 0; row < _grid.size(); row++) {
        bwCount = countPawnsInRow(row);
        if (std::max(bwCount.first, bwCount.second) > _grid.size()/2) {
            Takuzu::Pawn majorityPawnType = bwCount.first > bwCount.second ? Takuzu::Black : Takuzu::White;
            for (unsigned int column = 0; column < _grid.size(); column++) {
                if (areSameColor(majorityPawnType, _grid(row, column))) {
                    set.insert(std::make_pair(row, column));                 
                }
            }
        }
    }
}

void TakuzuGame::insertIllegalPawnsOnColumns(Takuzu::PawnsSet & set) const {
    std::pair<int, int> bwCount;
    for (unsigned int column = 0; column < _grid.size(); column++) {
        bwCount = countPawnsInColumn(column);
        if (std::max(bwCount.first, bwCount.second) > _grid.size()/2) {
            Takuzu::Pawn majorityPawnType = bwCount.first > bwCount.second ? Takuzu::Black : Takuzu::White;
            for (unsigned int row = 0; row < _grid.size(); row++) {
                if (areSameColor(majorityPawnType, _grid(row, column))) {
                    set.insert(std::make_pair(row, column));
                }
            }
        }
    }
}

void TakuzuGame::insertIllegalPawnsBetweenTwoPawnsOfTheSameColor(Takuzu::PawnsSet & set) const {
    for(unsigned int row = 0; row < _grid.size(); row++) {
        for(unsigned int column = 0; column < _grid.size(); column++) {
            if (_grid(row, column) != Takuzu::Empty) {
                if (_grid.notOnBorder(column) && isBetweenTwoPawnsOfTheSameColorInRow(row, column)) {
                    set.insert({std::make_pair(row, column), std::make_pair(row, column-1), std::make_pair(row, column+1)});
                }
                if (_grid.notOnBorder(row) && isBetweenTwoPawnsOfTheSameColorInColumn(row, column)) {
                    set.insert({std::make_pair(row, column), std::make_pair(row-1, column), std::make_pair(row+1, column)});
                }
            }
        }
    }
}

void TakuzuGame::insertIllegalPawnsInSimilarRows(Takuzu::PawnsSet & set) const {
    for (unsigned int row = 0; row < _grid.size(); row++) {
        for (unsigned int other = row+1; other < _grid.size(); other++) {
            if (areSameRow(row, other)) {
                for (unsigned int column = 0; column < _grid.size(); column++) {
                    set.insert(std::make_pair(row, column));
                    set.insert(std::make_pair(other, column));
                }
            }
        }
    }

}

void TakuzuGame::insertIllegalPawnsInSimilarColumns(Takuzu::PawnsSet & set) const {
    for (unsigned int column = 0; column < _grid.size(); column++) {
        for (unsigned int other = column+1; other < _grid.size(); other++) {
            if (areSameColumn(column, other)) {
                for (unsigned int row = 0; row < _grid.size(); row++) {
                    set.insert(std::make_pair(row, column));
                    set.insert(std::make_pair(row, other));
                }
            }
        }
    }

}


