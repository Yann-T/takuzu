# Projet de conception d'interface graphique

### Équipe :
Benjamin SAMUTH et Yann TRUPOT

## Objectif
L'objectif de ce projet était de concevoir une application graphique permettant à un utilisateur de jouer à une partie de Takuzu.

## Travail réalisé

### Les fonctionnalités implémentées :
Parmi les fonctionnalités suggérées voici celles que nous avons implémentées :
* Le choix de la difficulté et de la taille de la grille.  
* L'indication des colonnes/lignes correctes.  
* Le chronomètre indiquant le temps de jeu.  
* La mise en évidence des erreurs sur la grille.
* Le décompte du nombre de retours en arrière utilisé.  
* Indication du nombre de cases de chaque couleur pour chaque ligne/colonne.  
* Personnalisation des états des cases (avec des cercles ou des carrés).  

### Les fonctionnalités non implémentées : 
Parmis celles proposées, la seule que nous n'avons pas implémenté est l'implémentation de différents modes d'assistance.  
Outre celle-ci nous nous étions aussi fixé comme objectif à l'origine d'avoir deux modes de jeu. Un mode "normal", sans indication des erreurs et avec un décompte du nombre de retours en arrière (nous avions aussi imaginé une sorte de classement où seuls les résultats du mode normal aurait été pris en compte), ainsi qu'un mode "training" avec l'affichage des erreurs et sans chronomètre.  

### Les particularités de notre interface :
Notre interface reste assez basique, nous avons cependant fait en sorte de permettre à l'utilisateur de naviguer facilement dans les différents écrans (notamment à l'aide des boutons "retour").  
Nous avons aussi fait en sorte qu'une grande partie de l'application soit utilisable avec les raccourcis clavier.  

## La répartition des tâches : 
Benjamin :  
* Création des vues et présentations du menu principal et du menu de sélection de grille.  
* Création de la partie back-end du Takuzu (Détection des erreurs, chargement des fichiers...)
* Mise en place du chronomètre
* Mise en place du patron commande (pour les undo/redo).  

Yann :  
* Mise en place du système d'enchaînement des écrans.
* Création de la vue et de la présentation de l'écran de jeu.
* Création de la partie front-end du Takuzu (affichage de la grille ...).
* Création du menu Option.

## Choix effectués concernant l'ergonomie ;
* Permettre à l'utilisateur de facilement retourner en arrière
* Le menu option (ne servant actuellement qu'à changer l'apparence des cases) accessible sur toutes les fenêtres.

## Choix de modélisation : 
* Utilisation d'un patron MVP pour segmenter les différentes parties du programme.
* Utilisation du patron commande afin de permettre la mise en place des actions undo/redo.

